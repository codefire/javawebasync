/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author CodeFire
 */
@WebServlet("/say-hello")
public class AjaxServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String name = request.getParameter("username");
        if (name == null || name.trim().isEmpty()) {
            writer.write("<p style=\"color: red;\">Input correct name!</p>");
        } else {
            writer.printf("<p style=\"color: green;\">Hello, %s!</p>", name);
        }
        writer.flush();
    }
}
