<%-- 
    Document   : index
    Created on : Mar 26, 2015, 4:48:12 PM
    Author     : CodeFire
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Demo examples</h1>
        <h2>Send requests examples</h2>
        <ul>
            <li><a href="simple.jsp">Simple request</a></li>
            <li><a href="javascript.jsp">JavaScript request</a></li>
            <li><a href="jquery.jsp">JQuery request</a></li>
        </ul>
        <h2>Process requests examples</h2>
        <ul>
            <li><a href="simple">Simple servlet</a></li>
            <li><a href="async-simple">Simple async</a></li>
            <li><a href="async-advanced">Advanced async</a></li>
            <li><a href="async-long-running">Advanced async + listener</a></li>
            <li><a href="spring.jsp">SpringMVC async</a></li>
        </ul>
    </body>
</html>
